# SpNexa
SpNexa(SupportNexa) is a modern customer support platform tailored for system users. It offers a seamless and responsive support experience across various communication channels.

## Key Features:

### `Multichannel Support:`
SupportNexa enables users to seek assistance through multiple channels, including chat, email, and phone, ensuring accessibility and convenience.

### `Intelligent Routing:`
The platform employs intelligent routing algorithms to direct user inquiries to the most suitable support agents based on factors like language preference, account attributes, and interaction history.

### `Self-Service Options:`
SupportNexa includes self-service options such as an automated messaging experience (chatbot) on WhatsApp, empowering users to resolve common issues independently.

### `Sentiment Analysis:`
SupportNexa integrates sentiment analysis capabilities to analyze user inquiries and feedback, enabling better understanding of user sentiment and facilitating continuous improvement of support services.

# Tech Stack:

Backend: Python 3 (Django)
API Layer: GraphQL
Frontend:
    Web: TypeScript/React
Database: Postgres

#### `Please Note:`
Some features may not be present at the moment as they are still under development/review