import React, { useState } from 'react';
import { Box, Button, FormControl, FormLabel, Input, Textarea } from '@chakra-ui/react';

const EmailSupport: React.FC = () => {
  const [email, setEmail] = useState<string>('');
  const [subject, setSubject] = useState<string>('');
  const [message, setMessage] = useState<string>('');

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Here, you would implement the logic to send the email
    // For example, you can use an API to send the email to your support inbox
    console.log('Email sent:', { email, subject, message });
    // Reset form fields after submission
    setEmail('');
    setSubject('');
    setMessage('');
  };

  return (
    <Box maxW="600px" mx="auto" p="20px">
      <form onSubmit={handleSubmit}>
        <FormControl mb="20px">
          <FormLabel htmlFor="email">Your Email</FormLabel>
          <Input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </FormControl>
        <FormControl mb="20px">
          <FormLabel htmlFor="subject">Subject</FormLabel>
          <Input
            type="text"
            id="subject"
            value={subject}
            onChange={(e) => setSubject(e.target.value)}
            required
          />
        </FormControl>
        <FormControl mb="20px">
          <FormLabel htmlFor="message">Message</FormLabel>
          <Textarea
            id="message"
            value={message}
            onChange={(e) => setMessage(e.target.value)}
            required
          />
        </FormControl>
        <Button type="submit" colorScheme="blue">Send</Button>
      </form>
    </Box>
  );
};

export default EmailSupport;