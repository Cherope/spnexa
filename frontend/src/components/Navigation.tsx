import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Flex, Text } from '@chakra-ui/react';

const Navigation: React.FC = () => {
  return (
    <Flex as="nav" p="10px" bg="gray.200">
      <Box flex="1">
        <Link to="/" style={{ marginRight: '30px' }}>Home</Link>
        <Link to="/live-chat" style={{ marginRight: '30px' }}>Live-Chat</Link>
        <Link to="/email-support" style={{ marginRight: '30px' }}>Email-Support</Link>
        <Link to="/faq" style={{ marginRight: '30px' }}>FAQ</Link>
        {/* Add links for other channels */}
      </Box>
    </Flex>
  );
};

export default Navigation;