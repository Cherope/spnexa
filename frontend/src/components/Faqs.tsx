// src/components/FAQ.tsx
import React from 'react';
import { Box, Heading, Text } from '@chakra-ui/react';

const Faq: React.FC = () => {
    const faqData = [
        { question: "What is SpNexa?", answer: "SpNexa is a modern customer support platform tailored for system users." },
        { question: "How can I seek assistance?", answer: "You can seek assistance through multiple channels, including chat, email, and phone." },
      ];
      return (
        <Box maxW="600px" mx="auto" p="20px">
          <Heading as="h2" mb="20px">Frequently Asked Questions</Heading>
          {faqData.map((item, index) => (
            <Box key={index} mb="20px">
              <Heading as="h3" mb="5px">{item.question}</Heading>
              <Text>{item.answer}</Text>
            </Box>
          ))}
        </Box>
      );
};

export default Faq;