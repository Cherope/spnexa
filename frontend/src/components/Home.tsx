import React from 'react';
import { Link } from 'react-router-dom';
import { Box, Button, Heading } from '@chakra-ui/react';

const Home: React.FC = () => {
    return (
        <Box maxW="600px" mx="auto" p="20px">
          <Heading as="h2" mb="20px">Welcome to SupportNexa!</Heading>
          <Box mb="20px">
            <Heading as="h3" size="md" mb="10px">Choose your support channel:</Heading>
            <Button as={Link} to="/live-chat" colorScheme="green" mr="10px">Live Chat</Button>
            <Button as={Link} to="/email-support" colorScheme="teal"  mr="10px">Email Support</Button>
            <Button as={Link} to="/faq" colorScheme="blue">FAQ</Button>
          </Box>
        </Box>
      );
};

export default Home;