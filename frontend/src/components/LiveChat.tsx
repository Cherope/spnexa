import React, { useState } from 'react';
import { Box, Button, FormControl, FormLabel, Input, Text } from '@chakra-ui/react';

const LiveChat: React.FC = () => {
  const [messages, setMessages] = useState<string[]>([]);
  const [newMessage, setNewMessage] = useState<string>('');

  const handleMessageSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (newMessage.trim() !== '') {
      setMessages([...messages, newMessage]);
      setNewMessage('');
    }
  };

  return (
    <Box maxW="600px" mx="auto" p="20px">
      <Box mb="20px">
        {messages.map((message, index) => (
          <Text key={index} mb="10px">{message}</Text>
        ))}
      </Box>
      <form onSubmit={handleMessageSubmit}>
        <FormControl mb="20px">
          <FormLabel htmlFor="message">Your Message</FormLabel>
          <Input
            type="text"
            id="message"
            value={newMessage}
            onChange={(e) => setNewMessage(e.target.value)}
          />
        </FormControl>
        <Button type="submit" colorScheme="blue">Send</Button>
      </form>
    </Box>
  );
};

export default LiveChat;