import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ChakraProvider } from '@chakra-ui/react';
import Faq from './components/Faqs';
import Navigation from './components/Navigation';
import Home from './components/Home';
import LiveChat from './components/LiveChat';
import EmailSupport from './components/EmailSupport';
import './App.css';

function App() {
  return (
    <ChakraProvider>
      <Router>
        <Navigation />
        <Routes> {/* Use Routes instead of Switch */}
        <Route path="/" element={<Home />} />
        <Route path="/faq" element={<Faq />} />
        <Route path="/live-chat" element={<LiveChat />} />
        <Route path="/email-support" element={<EmailSupport />} />
          {/* Add routes for other channels */}
        </Routes>
      </Router>
    </ChakraProvider>
  );
}

export default App;
